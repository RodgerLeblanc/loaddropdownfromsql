loadDropDownFromSQL
======

Sample app written to help another dev.

Reads a SQL database and populate a DropDown with the data.

All done in QML.
